package com.galaxiatechsolutions.astanahussainia;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class weeklyDataRecyclerView {

    private String count, date, surah, username;

    public weeklyDataRecyclerView() {
        this.count = count;
        this.date = date;
        this.surah = surah;
        this.username = username;
    }

    public String getCount() {
        return count;
    }

    public String getDate() {
        return date;
    }

    public String getSurah() {
        return surah;
    }

    public String getUsername() {
        return username;
    }

    public void setCount( String count ) {
        this.count = count;
    }

    public void setDate( String date ) {
        this.date = date;
    }

    public void setSurah( String surah ) {
        this.surah = surah;
    }

    public void setUsername( String username ) {
        this.username = username;
    }
}
