package com.galaxiatechsolutions.astanahussainia;

public class Surah {

    private String id;
    private String surah;
    private String count;
    private String date;
    private String email;
    private String username;

    public Surah(){}

    public  Surah(String id, String surah, String count, String date ,String email, String username)
    {
        this.id = id;
        this.surah = surah;
        this.count = count;
        this.date = date;
        this.email = email;
        this.username = username;

    }

    public String getId() {
        return id;
    }

    public String getSurah() {
        return surah;
    }

    public String getCount() {
        return count;
    }

    public String getDate() {
        return date;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public void setSurah( String surah ) {
        this.surah = surah;
    }

    public void setCount( String count ) {
        this.count = count;
    }

    public void setDate( String date ) {
        this.date = date;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public void setUsername( String username ) {
        this.username = username;
    }
}
