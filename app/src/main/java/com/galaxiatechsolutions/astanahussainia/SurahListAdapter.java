package com.galaxiatechsolutions.astanahussainia;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SurahListAdapter extends RecyclerView.Adapter<SurahListAdapter.DataViewHolder> {
    Context context;
    List<Surah> surahList;

    public SurahListAdapter(Context context, List<Surah> surahList)
    {
        this.context = context;
        this.surahList = surahList;
    }


    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {

        View view = LayoutInflater.from(context).inflate(R.layout.countsurah_layout, parent, false);
        DataViewHolder dataViewHolder = new DataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder( @NonNull DataViewHolder dataViewHolder, int position ) {

        Surah surah = surahList.get(position);
        /*DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String strDate = dateFormat.format(date).toString();*/

        dataViewHolder.surah.setText(surah.getSurah());
        dataViewHolder.count.setText(surah.getCount());
        dataViewHolder.date.setText(surah.getDate());

    }

    @Override
    public int getItemCount() {
        return surahList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        TextView surah, count, date;


        public DataViewHolder( @NonNull View itemView ) {
            super(itemView);

            surah = itemView.findViewById(R.id.txtSurah);
            count = itemView.findViewById(R.id.txtCount);
            date = itemView.findViewById(R.id.txtDate);
        }
    }
}
