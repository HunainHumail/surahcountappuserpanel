package com.galaxiatechsolutions.astanahussainia;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class UserPanelActivity extends AppCompatActivity {
    Button btn_logout_user,btn_weekly__summary,btn_wazeefa_count;
    MainActivity mainActivity;
    TextView lbluser;
    FirebaseAuth mAuth;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient;




    @Override
    protected void onResume() {
        super.onResume();
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//        mGoogleApiClient = new GoogleApiClient.Builder(UserPanelActivity.this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
         String personEmail = acct.getEmail();
         String personDisplayName = acct.getDisplayName();

        lbluser = (TextView) findViewById(R.id.lbl_username);
     //   Intent i = getIntent();
     //   String i_name = i.getStringExtra("p_name");
        lbluser.setText(personDisplayName);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearApplicationData();

    }

    private void clearApplicationData() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseAuth.getInstance().signOut();
        sendToLogin();
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel);
        getSupportActionBar().hide();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
               .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(UserPanelActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        lbluser = (TextView) findViewById(R.id.lbl_username);
        Intent i = getIntent();
        String i_name = i.getStringExtra("p_name");
        lbluser.setText(i_name);



        btn_wazeefa_count = (Button) findViewById(R.id.btn_wazeefa_count_user);
        btn_wazeefa_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                Intent wazeefa_count = new Intent(getApplicationContext(),WazeefaCountingActivity.class);
                startActivity(wazeefa_count);

            }
        });


        btn_weekly__summary = (Button) findViewById(R.id.btn_data_summary_user);
        btn_weekly__summary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Intent weeklysummaryactivity = new Intent(getApplicationContext(),WeeklySummary.class);
                startActivity(weeklysummaryactivity);

            }
        });
        btn_logout_user = (Button) findViewById(R.id.btn_logout_user);
        btn_logout_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
               FirebaseAuth.getInstance().signOut();
               sendToLogin();
            }


        });



    }

    private void sendToLogin() { //funtion
        GoogleSignInClient mGoogleSignInClient ;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getBaseContext(), gso);
        mGoogleSignInClient.signOut().addOnCompleteListener(UserPanelActivity.this,
                new OnCompleteListener<Void>() {  //signout Google
                    @Override
                    public void onComplete( @NonNull Task<Void> task ) {
                        FirebaseAuth.getInstance().signOut(); //signout firebase
                        Intent setupIntent = new Intent(getBaseContext(), MainActivity.class);
                        Toast.makeText(getBaseContext(), "Logged Out", Toast.LENGTH_LONG).show(); //if u want to show some text
                        setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(setupIntent);
                        finish();
                    }


                });


    }
}

