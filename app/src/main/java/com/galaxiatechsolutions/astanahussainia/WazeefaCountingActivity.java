package com.galaxiatechsolutions.astanahussainia;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WazeefaCountingActivity extends AppCompatActivity {

    DatabaseReference myReference;
    RecyclerView wazeefaCountRecyclerView;
    List<Surah> surahList;
    Button btn_backToUserPanel;
    SurahListAdapter myAdapter;
    FloatingActionButton btn_addSurahDiaglog;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wazeefa_counting);

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        final String personEmail = acct.getEmail();

        wazeefaCountRecyclerView = findViewById(R.id.viewCountSurah);
        wazeefaCountRecyclerView.setHasFixedSize(true);
        wazeefaCountRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        surahList = new ArrayList<>();


        getSupportActionBar().hide();



        // OBJECT REFERENCE

        myReference = FirebaseDatabase.getInstance().getReference("surah");
        myReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange( @NonNull DataSnapshot dataSnapshot ) {
                surahList.clear();
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot surahSnapshot : dataSnapshot.getChildren())
                    {

                        Surah s = surahSnapshot.getValue(Surah.class);
                        if(s.getEmail().toString().equals(personEmail))
                        {
                            surahList.add(s);
                        }

                        else
                        {
                            continue;
                        }

                    }

                    myAdapter = new SurahListAdapter(WazeefaCountingActivity.this, surahList);
                    wazeefaCountRecyclerView.setAdapter(myAdapter);
                }
            }

            @Override
            public void onCancelled( @NonNull DatabaseError databaseError ) {

            }
        });

        // BACK BUTTON
        btn_backToUserPanel = (Button) findViewById(R.id.btn_backToUPanel);
        btn_backToUserPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                Intent userPanel = new Intent(getApplicationContext(), UserPanelActivity.class);
                startActivity(userPanel);
                finish();
            }
        });

        // FLOATING BUTTON
        btn_addSurahDiaglog = (FloatingActionButton) findViewById(R.id.floatingbtn_addwazeefa);
        btn_addSurahDiaglog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                showAddNewSurahDialog();
            }
        });
    }




    private void showAddNewSurahDialog()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Add New Surah");

        final View dialogView;
        LayoutInflater inflater = getLayoutInflater();

        dialogView = inflater.inflate(R.layout.dialoguebox_addsurah, null);

        final EditText surahText = (EditText) dialogView.findViewById(R.id.addsurah);
        final EditText countText = (EditText) dialogView.findViewById(R.id.addcount);
        //final TextView dateText = (TextView) dialogView.findViewById(R.id.txtDate);

            // Google Sign In was successful, authenticate with Firebase and getting user info

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                final String personEmail = acct.getEmail();
                final String personDisplayName = acct.getDisplayName();





        Button addSurah = (Button) dialogView.findViewById(R.id.buttonadd);
        dialogView.clearFocus();

        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();
        dialog.show();

        addSurah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {


                if (surahText.equals("") || countText.equals(""))// || dateText.equals(""))
                    Toast.makeText(WazeefaCountingActivity.this,"Please enter all fields",Toast.LENGTH_SHORT).show();
                else
                {
                    Surah mySurah = new Surah();

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    String strdate = dateFormat.format(date).toString();



                    mySurah.setSurah(surahText.getText().toString());
                    mySurah.setCount(countText.getText().toString());
                    mySurah.setDate(strdate);
                    mySurah.setEmail(personEmail.toString());
                    mySurah.setUsername(personDisplayName.toString());

                    AddSurahToFirebase(mySurah);
                    dialog.dismiss();


                }

            }
        });






    }

    private  void AddSurahToFirebase(Surah surah)
    {
        surah.setId(myReference.push().getKey());
        myReference.child(surah.getId()).setValue(surah);
        Toast.makeText(this, "Surah Added!", Toast.LENGTH_SHORT).show();
    }
}
