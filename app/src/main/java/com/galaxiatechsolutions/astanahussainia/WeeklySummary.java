 package com.galaxiatechsolutions.astanahussainia;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

 public class WeeklySummary extends AppCompatActivity {

     DatabaseReference myReference;
     Button btn_backToUserPanel;
     RecyclerView  recyclerView;
     weeklyDataAdapter weeklyDataAdapter;
     List<weeklyDataRecyclerView> weeklyDataRecyclerViewList;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_summary);
        getSupportActionBar().hide();
        btn_backToUserPanel = (Button) findViewById(R.id.btn_backToUPanel);
        btn_backToUserPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                Intent UserPanel = new Intent(getApplicationContext(),UserPanelActivity.class);
                startActivity(UserPanel);
                finish();
            }
        });

        recyclerView = findViewById(R.id.listWeeklyData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        weeklyDataRecyclerViewList = new ArrayList<>();

        myReference = FirebaseDatabase.getInstance().getReference("surah");
        myReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange( @NonNull DataSnapshot dataSnapshot ) {
                weeklyDataRecyclerViewList.clear();
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot weeklydataSnapshot : dataSnapshot.getChildren())
                    {
                        weeklyDataRecyclerView w = weeklydataSnapshot.getValue(weeklyDataRecyclerView.class);
                        weeklyDataRecyclerViewList.add(w);
                    }

                }

                weeklyDataAdapter = new weeklyDataAdapter(WeeklySummary.this, weeklyDataRecyclerViewList);
                recyclerView.setAdapter(weeklyDataAdapter);}

            @Override
            public void onCancelled( @NonNull DatabaseError databaseError ) {

            }
        });

        weeklyDataAdapter = new weeklyDataAdapter(this, weeklyDataRecyclerViewList);
        recyclerView.setAdapter(weeklyDataAdapter);


    }
}
