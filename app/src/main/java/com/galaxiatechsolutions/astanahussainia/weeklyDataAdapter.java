package com.galaxiatechsolutions.astanahussainia;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.List;

public class weeklyDataAdapter extends RecyclerView.Adapter<weeklyDataAdapter.DataViewHolder> {

    Context mCtx;
    List<weeklyDataRecyclerView> weeklyDataList;


    public weeklyDataAdapter( Context mCtx, List<weeklyDataRecyclerView> weeklyDataList ) {
        this.mCtx = mCtx;
        this.weeklyDataList = weeklyDataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {

        View view = LayoutInflater.from(mCtx).inflate(R.layout.weeklydata_layout, parent, false);
        DataViewHolder dataViewHolder = new DataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder( @NonNull DataViewHolder dataViewHolder, int position ) {
        weeklyDataRecyclerView weeklyDataRecyclerView = weeklyDataList.get(position);



//        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this.mCtx);
//        final String personName = acct.getDisplayName();


        dataViewHolder.date.setText(weeklyDataRecyclerView.getDate());
        dataViewHolder.username.setText(weeklyDataRecyclerView.getUsername());
        dataViewHolder.count.setText(weeklyDataRecyclerView.getCount());
//        Log.v("data",weeklyDataRecyclerView.getCounting().toString());
        dataViewHolder.surah.setText(weeklyDataRecyclerView.getSurah());

     //   Log.v("data",weeklyDataRecyclerView.getSurah().toString());

        //weeklyDataRecyclerView weeklyData = weeklyDataList.get(position);
        

    }

    @Override
    public int getItemCount() {
        return weeklyDataList.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder{

        TextView username, surah, count, date;

        public DataViewHolder( @NonNull View itemView ) {
            super(itemView);

            username = itemView.findViewById(R.id.txtUserNameDataSummary);
            surah = itemView.findViewById(R.id.txtSurahDataSummary);
            count = itemView.findViewById(R.id.txtCountingDataSummary);
            date = itemView.findViewById(R.id.txtDateDataSummary);
        }
    }
}
